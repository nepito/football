from os import listdir
import pandas as pd
import numpy as np
import json
from fastapi import FastAPI, Body


app = FastAPI()  # pragma: no mutate

LEAGUE = {
    "inglaterra": "39_2021",
    "francia": "61_2021",
    "alemania": "78_2021",
    "holanda": "88_2021",
    "portugal": "94_2021",
    "italia": "135_2021",
    "espagna": "140_2021",
}


@app.get("/{pais}")
def read_root(pais):
    league = LEAGUE[pais]
    predictions = pd.read_csv(f"data/statistics_{league}.csv")
    predictions["won"] = np.where(predictions["home"] < predictions["away"], "away", "draw")
    predictions["won"] = np.where(predictions["home"] > predictions["away"], "home", predictions["won"])
    return predictions.to_csv(index=False)


@app.get("/liga/{pais}")
def serie_a(pais):
    league = LEAGUE[pais]
    r = max([f.split("_")[-1].split(".")[0] for f in listdir("data/") if f.split("_")[1] == league])
    predictions = read_json(f"data/bets_{league}_2021_{r}.json")
    return predictions.to_json("records")
