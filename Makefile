module = futbol
codecov_token = ffcb865e-9822-446b-9387-ebf3c6ad30ed

results/data_file_262_2019.json: src/ejemplo_liga.py
	mkdir --parents results
	python src/ejemplo_liga.py

results/data_statisitcs_Monarcas_Atlas.json: src/ejemplo_stats_play.py
	mkdir --parents results
	python src/ejemplo_stats_play.py

results/statistics_262_2019.csv: src/league_statistics.py
	mkdir --parents results
	python src/league_statistics.py

.PHONY: check coverage format tests

check:
	black --check --line-length 100 ${module}
	black --check --line-length 100 tests
	black --check --line-length 100 src
	flake8 --max-line-length 100 ${module}
	flake8 --max-line-length 100 tests
	flake8 --max-line-length 100 src

clean:
	rm --force --recursive **/__pycache__
	rm --force --recursive __pycache__
	rm --force --recursive .pytest_cache
	rm --force --recursive results
	rm --force .mutmut-cache

coverage:
	mkdir --parents results
	pytest --cov=${module} --cov-report=xml --verbose && \
	codecov --token=${codecov_token}

format:
	black --line-length=100 ${module}
	black --line-length=100 src
	black --line-length=100 tests

init: install tests

install:
	pip install --editable .

mutants:
	mkdir --parents results
	mutmut run --paths-to-mutate ${module}

test_cli:
	shellspec

tests:
	mkdir --parents results
	pytest --verbose

last_results_ligue_1_2020:
	mkdir --parents results
	python src/ejemplo_liga.py --season=2020 --league=61

ligue_1_2020: last_results_ligue_1_2020
	mkdir --parents results/ligue_1_2020/
	python src/get_fixture_from_id_of_league.py --id-season=61_2020

update_ligue_1_2020: ligue_1_2020
	mkdir --parents results
	python src/statistics_from_league_matches.py --id-season=61_2020
	python src/penalties_from_players.py --league-season=61_2020
	python src/join_penalties.py --league-season=61_2020

last_results_bundesliga_2021:
	mkdir --parents results
	python src/ejemplo_liga.py --season=2021 --league=78

bundesliga_2021: last_results_bundesliga_2021
	mkdir --parents results/Bundesliga_2021/
	python src/get_fixture_from_id_of_league.py --id-season=78_2021

update_bundesliga_2021: bundesliga_2021
	mkdir --parents results
	python src/statistics_from_league_matches.py --id-season=78_2021
	python src/penalties_from_players.py --league-season=78_2021
	python src/join_penalties.py --league-season=78_2021

last_results_serie_a_2021:
	mkdir --parents results
	python src/ejemplo_liga.py --season=2021 --league=135

serie_a_2021: last_results_serie_a_2021
	mkdir --parents results/serie_a_2021/
	python src/get_fixture_from_id_of_league.py --id-season=135_2021

update_serie_a_2021: serie_a_2021
	mkdir --parents results
	python src/statistics_from_league_matches.py --id-season=135_2021
	python src/penalties_from_players.py --league-season=135_2021
	python src/join_penalties.py --league-season=135_2021

last_results_premier_league_2020:
	mkdir --parents results
	python src/ejemplo_liga.py --season=2020 --league=39

premier_league_2020: last_results_premier_league_2020
	mkdir --parents results/premier_league_2020/
	python src/get_fixture_from_id_of_league.py --id-season=39_2020

update_premier_league_2020: premier_league_2020
	mkdir --parents results
	python src/statistics_from_league_matches.py --id-season=39_2020

last_results_premier_league_2021:
	mkdir --parents results
	python src/ejemplo_liga.py --season=2021 --league=39

premier_league_2021: last_results_premier_league_2021
	mkdir --parents results/premir_league_2021/
	python src/get_fixture_from_id_of_league.py --id-season=39_2021

update_premier_league_2021: premier_league_2021
	mkdir --parents results
	python src/statistics_from_league_matches.py --id-season=39_2021
	python src/penalties_from_players.py --league-season=39_2021
	python src/join_penalties.py --league-season=39_2021

last_results_ligue_1_2021:
	mkdir --parents results
	python src/ejemplo_liga.py --season=2021 --league=61

ligue_1_2021: last_results_ligue_1_2021
	mkdir --parents results/ligue_1_2021/
	python src/get_fixture_from_id_of_league.py --id-season=61_2021

update_ligue_1_2021: ligue_1_2021
	mkdir --parents results
	python src/statistics_from_league_matches.py --id-season=61_2021
	python src/penalties_from_players.py --league-season=61_2021
	python src/join_penalties.py --league-season=61_2021

la_liga_2020: last_results_la_liga_2020
	mkdir --parents results/la_liga_2020/
	python src/get_fixture_from_id_of_league.py --id-season=140_2020

update_la_liga_2020: la_liga_2020
	mkdir --parents results
	python src/statistics_from_league_matches.py --id-season=140_2020

last_results_la_liga_2021:
	mkdir --parents results
	python src/ejemplo_liga.py --season=2021 --league=140

la_liga_2021: last_results_la_liga_2021
	mkdir --parents results/la_liga_2021/
	python src/get_fixture_from_id_of_league.py --id-season=140_2021

update_la_liga_2021: la_liga_2021
	mkdir --parents results
	python src/statistics_from_league_matches.py --id-season=140_2021
	python src/penalties_from_players.py --league-season=140_2021
	python src/join_penalties.py --league-season=140_2021

bundesligue_2020: results/data_file_78_2020.json
	mkdir --parents results/Bundesliga_2020/
	python src/bundesliga_get_id_fixture.py

results/data_file_78_2020.json:
	mkdir --parents results
	python src/ejemplo_liga.py --season=2020 --league=78

last_results_mx:
	mkdir --parents results
	python src/ejemplo_liga.py --season=2021 --league=262

mx_2021: last_results_mx
	mkdir --parents results/MX_2021/
	python src/get_fixture_from_id_of_league.py --id-season=262_2021

update_mx_2021: mx_2021
	mkdir --parents results
	python src/statistics_from_league_matches.py
	python src/penalties_from_players.py --league-season=262_2021
	python src/join_penalties.py --league-season=262_2021

serie_a_2020: results/data_file_78_2020.json
	mkdir --parents results/serie_a_2020/
	python src/get_fixture_from_id_of_league.py

last_results_Eredivisie_2020:
	mkdir --parents results
	python src/ejemplo_liga.py --season=2020 --league=88

Eredivisie_2020: last_results_Eredivisie_2020
	mkdir --parents results/Eredivisie_2020/
	python src/get_fixture_from_id_of_league.py --id-season=88_2020

update_Eredivisie_2020: Eredivisie_2020
	mkdir --parents results
	python src/statistics_from_league_matches.py --id-season=88_2020
	python src/penalties_from_players.py --league-season=88_2020
	python src/join_penalties.py --league-season=88_2020

last_results_Eredivisie_2021:
	mkdir --parents results
	python src/ejemplo_liga.py --season=2021 --league=88

Eredivisie_2021: last_results_Eredivisie_2021
	mkdir --parents results/Eredivisie_2021/
	python src/get_fixture_from_id_of_league.py --id-season=88_2021 --no-is-free

update_Eredivisie_2021: Eredivisie_2021
	mkdir --parents results
	python src/statistics_from_league_matches.py --id-season=88_2021
	python src/penalties_from_players.py --league-season=88_2021
	python src/join_penalties.py --league-season=88_2021

last_results_primeira_liga_2020:
	mkdir --parents results
	python src/ejemplo_liga.py --season=2020 --league=94

primeira_liga_2020: last_results_primeira_liga_2020
	mkdir --parents results/primeira_liga_2020/
	python src/get_fixture_from_id_of_league.py --id-season=94_2020

update_primeira_liga_2020: primeira_liga_2020
	mkdir --parents results
	python src/statistics_from_league_matches.py --id-season=94_2020
	python src/penalties_from_players.py --league-season=94_2020
	python src/join_penalties.py --league-season=94_2020

last_results_primeira_liga_2021:
	mkdir --parents results
	python src/ejemplo_liga.py --season=2021 --league=94

primeira_liga_2021: last_results_primeira_liga_2021
	mkdir --parents results/primeira_liga_2021/
	python src/get_fixture_from_id_of_league.py --id-season=94_2021

update_primeira_liga_2021: primeira_liga_2021
	mkdir --parents results
	python src/statistics_from_league_matches.py --id-season=94_2021
	python src/penalties_from_players.py --league-season=94_2021
	python src/join_penalties.py --league-season=94_2021

last_results_mls_2021:
	mkdir --parents results
	python src/ejemplo_liga.py --season=2021 --league=253

mls_2021: last_results_mls_2021
	mkdir --parents results/mls_2021/
	python src/get_fixture_from_id_of_league.py --id-season=253_2021

update_mls_2021: mls_2021
	mkdir --parents results
	python src/statistics_from_league_matches.py --id-season=253_2021
	python src/penalties_from_players.py --league-season=253_2021
	python src/join_penalties.py --league-season=253_2021

update_2021: \
	update_premier_league_2021 \
	update_serie_a_2021 \
	update_primeira_liga_2021 \
	update_Eredivisie_2021 \
	update_la_liga_2021 \
	update_bundesliga_2021 \
	update_ligue_1_2021
