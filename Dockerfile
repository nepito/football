FROM python:3.10
WORKDIR /workdir
COPY . .
RUN pip install \
    . \
    black \
    codecov \
    flake8 \
    mutmut \
    pylint \
    pydantic \
    pytest \
    pytest-cov \
    pytest-mock \
    requests-mock \
    rope
RUN make install
