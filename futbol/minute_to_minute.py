NAME_FILE = "minute_minute"


def array_dictionary_2_dictionary(array: list) -> dict:
    return {element["type"]: element["value"] for element in array}


def get_home_statistics(data: dict) -> dict:
    return {
        "match": data["parameters"]["fixture"],
        "team": data["response"][0]["team"]["id"],
        "home": True,
    }


def get_away_statistics(data: dict) -> dict:
    return {
        "match": data["parameters"]["fixture"],
        "team": data["response"][1]["team"]["id"],
        "home": False,
    }
