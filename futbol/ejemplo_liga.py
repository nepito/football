import os
from os import listdir
from os.path import isfile, join
import json
import requests


def get_rapidkey():
    rapidapi_key = os.environ["x_rapidapi_key"]
    return rapidapi_key


def get_headers():
    rapidapi_key = get_rapidkey()
    headers = {"x-rapidapi-host": "v3.football.api-sports.io", "x-rapidapi-key": rapidapi_key}
    return headers


def read_json(file_name):
    f = open(
        file_name,
    )
    league = json.load(f)
    f.close()
    return league


def get_id_played_match(league):
    ids = [
        match["fixture"]["id"]
        for match in league["response"]
        if match["fixture"]["status"]["long"] == "Match Finished"
    ]
    return ids


def downladed_file(my_path):
    onlyfiles = [f for f in listdir(my_path) if isfile(join(my_path, f))]
    fixture = [int(f.split(".")[0].split("_")[-1]) for f in onlyfiles if f.split(".")[1] == "json"]
    fixture = list(set(fixture))
    return fixture


def read_statistic_match(match_path):
    match_file = open(match_path)
    statistic_of_match = json.load(match_file)
    match_file.close()
    return statistic_of_match


class Downloader_Matches:
    def __init__(self):
        self.headers = get_headers()
        self.answer = None
        self.match = None

    def get_request(self, match):
        self.match = match
        conn = requests.get(
            f"https://v3.football.api-sports.io/fixtures/statistics?fixture={self.match}",
            headers=self.headers,
        )
        self.answer = conn.json()

    def write_match(self, directory_path):
        output = f"results/{directory_path}/data_statisitcs_{self.match}.json"
        if self.answer["results"] == 2:
            with open(output, "w") as write_file:
                json.dump(self.answer, write_file)


class Downloader_Matches_Minute_Minute:
    def __init__(self):
        self.headers = get_headers()
        self.answer = None
        self.match = None

    def get_request(self, match):
        self.match = match
        conn = requests.get(
            f"https://v3.football.api-sports.io/fixtures/statistics?fixture={self.match}",
            headers=self.headers,
        )
        self.answer = conn.json()

    def write_match(self, directory_path, minute):
        output = f"results/{directory_path}/data_statisitcs_{self.match}_{minute}.json"
        if self.answer["results"] == 2:
            with open(output, "w") as write_file:
                json.dump(self.answer, write_file)


class Downloader_Players:
    def __init__(self):
        self.headers = get_headers()
        self.answer = None
        self.match = None

    def get_request(self, match):
        self.match = match
        conn = requests.get(
            f"https://v3.football.api-sports.io/fixtures/players?fixture={self.match}",
            headers=self.headers,
        )
        self.answer = conn.json()

    def write_players(self, directory_path):
        output = f"results/{directory_path}/data_statisitcs_players_{self.match}.json"
        if self.answer["results"] == 2:
            with open(output, "w") as write_file:
                json.dump(self.answer, write_file)


ID_LEAGUE_SEASON = {
    "2_2021": "Champions_2021",
    "61_2020": "ligue_1_2020",
    "61_2021": "ligue_1_2021",
    "78_2020": "Bundesliga_2020",
    "78_2021": "Bundesliga_2021",
    "39_2020": "premier_league_2020",
    "39_2021": "premir_league_2021",
    "94_2020": "primeira_liga_2020",
    "94_2021": "primeira_liga_2021",
    "135_2020": "serie_a_2020",
    "135_2021": "serie_a_2021",
    "140_2020": "la_liga_2020",
    "140_2021": "la_liga_2021",
    "262_2021": "MX_2021",
    "262_2020": "MX_2020",
    "88_2020": "Eredivisie_2020",
    "88_2021": "Eredivisie_2021",
    "197_2020": "greek_sl_2020",
    "197_2021": "greek_sl_2021",
    "207_2020": "swiss_sl_2020",
    "207_2021": "swiss_sl_2021",
    "253_2021": "mls_2021",
}
