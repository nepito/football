"""Package to download data football"""

__version__ = "0.5.0"
from .ejemplo_liga import *  # noqa
from .calculate_tsr_from_play import Match  # noqa
from .calculate_tsr_from_play import League  # noqa
from .calculate_tsr_from_play import Team  # noqa
from .minute_to_minute import *  # noqa
from .matches_of_seasons import *  # noqa
from .passes_allowed_per_defensive_actions import *  # noqa
from .statistics_of_players import *  # noqa
