from pydantic import BaseModel


class Match_Round(BaseModel):
    id_home: int
    id_away: int
    round: str


def return_dictionary_from_match(match):
    variables = {
        "id_home": match["teams"]["home"]["id"],
        "id_away": match["teams"]["away"]["id"],
        "round": match["league"]["round"],
    }
    return variables


def return_dictionary_from_season(season):
    all_data = {
        "home_id": [match.id_home for match in season],
        "away_id": [match.id_away for match in season],
        "round": [match.round for match in season],
    }
    return all_data
