import pandas as pd


def sum_summary_of_passes_and_interceptions():
    path = "tests/data/statisitcs_players_39_2021.csv"
    players = pd.read_csv(path)
    return (
        players.groupby(["match", "team"])
        .agg(
            passes=pd.NamedAgg(column="passes_accuracy", aggfunc="sum"),
            interceptions=pd.NamedAgg(column="tackles_interceptions", aggfunc="sum"),
        )
        .reset_index()
    )
