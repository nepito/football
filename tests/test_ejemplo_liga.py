import requests_mock
from futbol import (
    downladed_file,
    Downloader_Matches,
    Downloader_Matches_Minute_Minute,
    Downloader_Players,
    get_rapidkey,
    get_headers,
    ID_LEAGUE_SEASON,
    read_json,
    get_id_played_match,
    read_statistic_match,
)
import os


def test_get_rapidkey():
    obtained_key = get_rapidkey()
    expected_key = os.environ["x_rapidapi_key"]
    assert expected_key == obtained_key


def test_get_headers(mocker):
    def get_rapidkey():
        return "key"

    mocker.patch("futbol.ejemplo_liga.get_rapidkey", get_rapidkey)
    obtained_headers = get_headers()
    expected_headers = {"x-rapidapi-host": "v3.football.api-sports.io", "x-rapidapi-key": "key"}
    assert expected_headers == obtained_headers


expected_ID_LEAGUE_SEASON = {
    "2_2021": "Champions_2021",
    "61_2020": "ligue_1_2020",
    "61_2021": "ligue_1_2021",
    "78_2020": "Bundesliga_2020",
    "78_2021": "Bundesliga_2021",
    "39_2020": "premier_league_2020",
    "39_2021": "premir_league_2021",
    "94_2020": "primeira_liga_2020",
    "94_2021": "primeira_liga_2021",
    "135_2020": "serie_a_2020",
    "135_2021": "serie_a_2021",
    "140_2020": "la_liga_2020",
    "140_2021": "la_liga_2021",
    "262_2021": "MX_2021",
    "262_2020": "MX_2020",
    "88_2020": "Eredivisie_2020",
    "88_2021": "Eredivisie_2021",
    "197_2020": "greek_sl_2020",
    "197_2021": "greek_sl_2021",
    "207_2020": "swiss_sl_2020",
    "207_2021": "swiss_sl_2021",
}


def test_ID_LEAGUE_SEASON():
    assert expected_ID_LEAGUE_SEASON == ID_LEAGUE_SEASON


def test_get_id_played_match():
    expected_id_played_match = [169101, 169102, 169103, 169104, 169105]
    league = read_json("tests/data/data_file_262_2019.json")
    obtained_id_played_match = get_id_played_match(league)
    assert expected_id_played_match == obtained_id_played_match


expected_match = {
    "get": "fixtures/statistics",
    "parameters": {"fixture": "169101"},
    "errors": [],
    "results": 2,
    "paging": {"current": 1, "total": 1},
    "response": [
        {
            "team": {
                "id": 2290,
                "name": "Club Queretaro",
                "logo": "https://media.api-sports.io/football/teams/2290.png",
            },
            "statistics": [
                {"type": "Shots on Goal", "value": 7},
                {"type": "Shots off Goal", "value": 10},
                {"type": "Total Shots", "value": 20},
                {"type": "Blocked Shots", "value": 3},
                {"type": "Shots insidebox", "value": 12},
                {"type": "Shots outsidebox", "value": 8},
                {"type": "Fouls", "value": 15},
                {"type": "Corner Kicks", "value": 6},
                {"type": "Offsides", "value": 2},
                {"type": "Ball Possession", "value": "70%"},
                {"type": "Yellow Cards", "value": 1},
                {"type": "Red Cards", "value": None},
                {"type": "Goalkeeper Saves", "value": 3},
                {"type": "Total passes", "value": 484},
                {"type": "Passes accurate", "value": 431},
                {"type": "Passes %", "value": "89%"},
            ],
        },
        {
            "team": {
                "id": 2292,
                "name": "Pachuca",
                "logo": "https://media.api-sports.io/football/teams/2292.png",
            },
            "statistics": [
                {"type": "Shots on Goal", "value": 4},
                {"type": "Shots off Goal", "value": 6},
                {"type": "Total Shots", "value": 14},
                {"type": "Blocked Shots", "value": 4},
                {"type": "Shots insidebox", "value": 4},
                {"type": "Shots outsidebox", "value": 10},
                {"type": "Fouls", "value": 17},
                {"type": "Corner Kicks", "value": 2},
                {"type": "Offsides", "value": None},
                {"type": "Ball Possession", "value": "30%"},
                {"type": "Yellow Cards", "value": 3},
                {"type": "Red Cards", "value": 1},
                {"type": "Goalkeeper Saves", "value": 5},
                {"type": "Total passes", "value": 214},
                {"type": "Passes accurate", "value": 156},
                {"type": "Passes %", "value": "73%"},
            ],
        },
    ],
}


def test_read_statistic_match():
    obtained_match = read_statistic_match("tests/data/data_statisitcs_169101.json")
    assert expected_match == obtained_match


def test_downladed_file():
    my_path = "tests/data/"
    expected_id = [2019, *[i for i in range(169101, 169106)], 718522]
    obtained_id = downladed_file(my_path)
    assert expected_id == obtained_id


@requests_mock.Mocker(kw="mock")
def test_Downloader_Matches(**kwargs):
    url = "https://v3.football.api-sports.io/fixtures/statistics?fixture=26262"
    expected_answer = {"results": 2}
    kwargs["mock"].get(url, json=expected_answer)
    dm = Downloader_Matches()
    assert dm.answer is None
    assert dm.match is None
    dm.get_request(match=26262)
    assert dm.headers["x-rapidapi-host"] == "v3.football.api-sports.io"
    assert dm.answer == expected_answer
    os.makedirs("results/tests", exist_ok=True)
    _remove_file_results_tests_data_statisitcs("")
    assert not os.path.exists("results/tests/data_statisitcs_26262.json")
    dm.write_match("tests")
    assert os.path.exists("results/tests/data_statisitcs_26262.json")
    _remove_file_results_tests_data_statisitcs("")


@requests_mock.Mocker(kw="mock")
def test_Downloader_Players(**kwargs):
    url = "https://v3.football.api-sports.io/fixtures/players?fixture=26262"
    expected_answer = {"results": 2}
    kwargs["mock"].get(url, json=expected_answer)
    dp = Downloader_Players()
    assert dp.answer is None
    assert dp.match is None
    dp.get_request(match=26262)
    assert dp.headers["x-rapidapi-host"] == "v3.football.api-sports.io"
    assert dp.answer == expected_answer
    os.makedirs("results/tests", exist_ok=True)
    _remove_file_results_tests_data_statisitcs("_players")
    assert not os.path.exists("results/tests/data_statisitcs_players_26262.json")
    dp.write_players("tests")
    assert os.path.exists("results/tests/data_statisitcs_players_26262.json")
    _remove_file_results_tests_data_statisitcs("_players")


def _remove_file_results_tests_data_statisitcs(path="", minute=""):
    fill_path = f"results/tests/data_statisitcs{path}_26262{minute}.json"
    if os.path.exists(fill_path):
        os.remove(fill_path)


@requests_mock.Mocker(kw="mock")
def test_Downloader_Matches_Minute_Minute(**kwargs):
    url = "https://v3.football.api-sports.io/fixtures/statistics?fixture=26262"
    expected_answer = {"results": 2}
    kwargs["mock"].get(url, json=expected_answer)
    d3m = Downloader_Matches_Minute_Minute()
    assert d3m.answer is None
    assert d3m.match is None
    d3m.get_request(match=26262)
    assert d3m.headers["x-rapidapi-host"] == "v3.football.api-sports.io"
    assert d3m.answer == expected_answer
    os.makedirs("results/tests", exist_ok=True)
    _remove_file_results_tests_data_statisitcs(minute="_3")
    assert not os.path.exists("results/tests/data_statisitcs_26262_3.json")
    d3m.write_match("tests", 3)
    assert os.path.exists("results/tests/data_statisitcs_26262_3.json")
    _remove_file_results_tests_data_statisitcs(minute="_3")
