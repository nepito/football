import pytest
from futbol import (
    NAME_FILE,
    array_dictionary_2_dictionary,
    read_json,
    get_home_statistics,
    get_away_statistics,
)


def test_name_file():
    obtained = NAME_FILE
    expected = "minute_minute"
    assert expected == obtained


testdata = [
    pytest.param(
        {"a": 1, "b": 2}, [{"type": "a", "value": 1}, {"type": "b", "value": 2}], id="a y b"
    ),
    pytest.param(
        {"b": 2, "c": 3}, [{"type": "b", "value": 2}, {"type": "c", "value": 3}], id="b y c"
    ),
    pytest.param(
        {"a": 1, "b": 2, "c": 3},
        [
            {"type": "a", "value": 1},
            {"type": "b", "value": 2},
            {"type": "c", "value": 3},
        ],
        id="a, b y c",
    ),
]


@pytest.mark.parametrize("expected, array_2_change", testdata)
def test_array_dictionary_2_dictionary(expected, array_2_change):
    obtained = array_dictionary_2_dictionary(array_2_change)
    assert expected == obtained


data = read_json("tests/data/data_statisitcs_169105.json")


def test_get_home_statistics():
    expected_match = "169105"
    obtained = get_home_statistics(data)
    assert expected_match == obtained["match"]
    expected_team = 2281
    assert expected_team == obtained["team"]
    expected_home = True
    assert expected_home == obtained["home"]


def test_get_away_statistics():
    expected_match = "169105"
    obtained = get_away_statistics(data)
    assert expected_match == obtained["match"]
    expected_team = 2287
    assert expected_team == obtained["team"]
    expected_home = False
    assert expected_home == obtained["home"]
