from futbol import (
    return_dictionary_from_match,
    return_dictionary_from_season,
    read_json,
    Match_Round,
)

EXAMPLE_MATCH = {
    "fixture": {
        "id": 571472,
        "referee": "J. Hamel",
        "timezone": "UTC",
        "date": "2020-08-23T13:00:00+00:00",
        "timestamp": 1598187600,
        "periods": {"first": 1598187600, "second": 1598191200},
        "venue": {"id": 656, "name": "Stade Yves Allainmat - Le Moustoir", "city": "Lorient"},
        "status": {"long": "Match Finished", "short": "FT", "elapsed": 90},
    },
    "league": {
        "id": 61,
        "name": "Ligue 1",
        "country": "France",
        "logo": "https://media.api-sports.io/football/leagues/61.png",
        "flag": "https://media.api-sports.io/flags/fr.svg",
        "season": 2020,
        "round": "Regular Season - 1",
    },
    "teams": {
        "home": {
            "id": 97,
            "name": "Lorient",
            "logo": "https://media.api-sports.io/football/teams/97.png",
        },
        "away": {
            "id": 95,
            "name": "Strasbourg",
            "logo": "https://media.api-sports.io/football/teams/95.png",
        },
    },
    "goals": {"home": 3, "away": 1},
    "score": {
        "halftime": {"home": 0, "away": 1},
        "fulltime": {"home": 3, "away": 1},
    },
}


def test_return_dictionary_from_match():
    obtained_dictionary_from_match = return_dictionary_from_match(EXAMPLE_MATCH)
    expected_dictionary_from_match = {
        "id_home": 97,
        "id_away": 95,
        "round": "Regular Season - 1",
    }
    assert expected_dictionary_from_match == obtained_dictionary_from_match


league_season = "262_2019"
path_season = f"tests/data/data_file_{league_season}.json"
season = read_json(path_season)
matches = season["response"]
season = [Match_Round(**return_dictionary_from_match(match)) for match in matches]


def test_return_dictionary_from_season():
    obtained_dictionary = return_dictionary_from_season(season)
    expected_dictionary = {
        "home_id": [2290, 2295, 2279, 2278, 2281],
        "away_id": [2292, 2298, 2288, 2314, 2287],
        "round": ["Apertura - 4", "Apertura - 4", "Apertura - 4", "Apertura - 4", "Apertura - 4"],
    }
    assert expected_dictionary == obtained_dictionary
