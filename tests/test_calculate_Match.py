from futbol import Match, Team, League
from pandas._testing import assert_frame_equal
import pandas as pd
import json
import pytest

league_path = "tests/data/data_file_262_2019.json"
league_file = open(
    league_path,
)

statistic_of_league = json.load(league_file)
league_file.close()

match_path = "tests/data/data_statisitcs_169104.json"
match_file = open(
    match_path,
)
statistic_of_match_169104 = json.load(match_file)
match_file.close()

match_path = "tests/data/data_statisitcs_169105.json"
match_file = open(
    match_path,
)
statistic_of_match_169105 = json.load(match_file)
match_file.close()


class Test_League:
    def setup(self):
        self.league = League(statistic_of_league)
        self.empty = {
            "match_id": [],
            "home_id": [],
            "home_total_shots": [],
            "home_shots_on_goal": [],
            "away_id": [],
            "away_total_shots": [],
            "away_shots_on_goal": [],
            "home": [],
            "away": [],
        }

    def test_league_info(self):
        expected_id = 262
        obtained_id = self.league.id
        assert expected_id == obtained_id
        expected_season = 2019
        obtained_season = self.league.season
        assert expected_season == obtained_season

    def test_id_match(self):
        expected_id_matches = [169101, 169102, 169103, 169104, 169105]
        obtained_id_matches = self.league.id_matches
        assert expected_id_matches == obtained_id_matches

    def test_goals(self):
        expected_goals = {
            169101: {"home": 2, "away": 1},
            169102: {"home": 2, "away": 0},
            169103: {"home": 3, "away": 1},
            169104: {"home": 3, "away": 0},
            169105: {"home": 0, "away": 1},
        }
        obtained_goals = self.league.goals
        assert expected_goals == obtained_goals

    def test_match_2_dataframe(self):
        expected_empty_dataframe = pd.DataFrame(self.empty)
        obtained_empty_dataframe = self.league.statistics
        assert_frame_equal(expected_empty_dataframe, obtained_empty_dataframe)

    def test_fill_match_statistic(self):
        expected_summary_match = {
            "home": 0,
            "away": 1,
            "match_id": 169105,
            "home_id": 2281,
            "home_total_shots": 9,
            "home_shots_on_goal": 2,
            "home_shots_insidebox": 7,
            "away_id": 2287,
            "away_total_shots": 9,
            "away_shots_on_goal": 2,
            "away_shots_insidebox": 1,
        }
        obtained_summary_match = self.league.fill_match_statistic(statistic_of_match_169105)
        assert expected_summary_match == obtained_summary_match
        expected_summary_match = {
            "home": 3,
            "away": 0,
            "match_id": 169104,
            "home_id": 2278,
            "home_total_shots": 13,
            "home_shots_on_goal": 6,
            "home_shots_insidebox": 9,
            "away_id": 2314,
            "away_total_shots": 17,
            "away_shots_on_goal": 3,
            "away_shots_insidebox": 8,
        }
        obtained_summary_match = self.league.fill_match_statistic(statistic_of_match_169104)
        assert expected_summary_match == obtained_summary_match

    def test_append_match_statistic(self):
        expected_summary_match = {
            "home": [0, 3],
            "away": [1, 0],
            "match_id": [169105, 169104],
            "home_id": [2281, 2278],
            "home_total_shots": [9, 13],
            "home_shots_on_goal": [2, 6],
            "home_shots_insidebox": [7, 9],
            "away_id": [2287, 2314],
            "away_total_shots": [9, 17],
            "away_shots_on_goal": [2, 3],
            "away_shots_insidebox": [1, 8],
        }
        expected_dataframe = pd.DataFrame(expected_summary_match)
        self.league.append_match_statistic(statistic_of_match_169105)
        self.league.append_match_statistic(statistic_of_match_169104)
        obtained_dataframe = self.league.statistics
        assert_frame_equal(expected_dataframe, obtained_dataframe, check_dtype=False)

    def test_write_statistics(self, mocker):
        to_csv = mocker.spy(pd.DataFrame, "to_csv")
        self.league.write_statistics("results/borrame.csv")
        args, kwargs = to_csv.call_args
        assert kwargs == {"index": False}


class Test_Match:
    def setup(self):
        self.match = Match(statistic_of_match_169105)

    def test_id(self):
        expected_id = 169105
        obtained_id = self.match.id
        assert expected_id == obtained_id

    def test_id_team(self):
        expected_home_team_id = 2281
        obtained_home_team_id = self.match.home_team.id
        assert expected_home_team_id == obtained_home_team_id
        expected_away_team_id = 2287
        obtained_away_team_id = self.match.away_team.id
        assert expected_away_team_id == obtained_away_team_id

    def test_summary_match(self):
        expected_summary_match = {
            "match_id": 169105,
            "home_id": 2281,
            "home_total_shots": 9,
            "home_shots_on_goal": 2,
            "home_shots_insidebox": 7,
            "away_id": 2287,
            "away_total_shots": 9,
            "away_shots_on_goal": 2,
            "away_shots_insidebox": 1,
        }
        obtained_summary_match = self.match.summary()
        assert expected_summary_match == obtained_summary_match


statistic_home_team = statistic_of_match_169105["response"][0]
statistic_away_team = statistic_of_match_169105["response"][1]


class Test_Team:
    @pytest.mark.parametrize(
        "expected, team", [(2281, statistic_home_team), (2287, statistic_away_team)]
    )
    def test_id(self, expected, team):
        self._assert_id(expected, team)

    def test_total_shots(self):
        expected_total_shots = 9
        self._assert_total_shots(expected_total_shots, statistic_home_team)
        expected_total_shots = 9
        self._assert_total_shots(expected_total_shots, statistic_away_team)

    def test_shots_on_goal(self):
        expected_shots_on_goal = 2
        self._assert_shots_on_goal(expected_shots_on_goal, statistic_home_team)
        expected_shots_on_goal = 2
        self._assert_shots_on_goal(expected_shots_on_goal, statistic_away_team)

    def test_shots_insidebox(self):
        expected_shots_insidebox = 7
        self._assert_shots_insidebox(expected_shots_insidebox, statistic_home_team)
        expected_shots_insidebox = 1
        self._assert_shots_insidebox(expected_shots_insidebox, statistic_away_team)

    def test_summary_team(self):
        expected_summary_team = {
            "id": 2281,
            "total_shots": 9,
            "shots_on_goal": 2,
            "shots_insidebox": 7,
        }
        team = Team(statistic_home_team)
        obtained_summary_team = team.summary()
        assert expected_summary_team == obtained_summary_team

    def _assert_id(self, expected, statistic):
        team = Team(statistic)
        obtained_id = team.id
        assert expected == obtained_id

    def _assert_total_shots(self, expected, statistic):
        team = Team(statistic)
        obtained = team.total_shots
        assert expected == obtained

    def _assert_shots_on_goal(self, expected, statistic):
        team = Team(statistic)
        obtained_shots_on_goal = team.shots_on_goal
        assert expected == obtained_shots_on_goal

    def _assert_shots_insidebox(self, expected, statistic):
        team = Team(statistic)
        obtained = team.shots_insidebox
        assert expected == obtained
