from typer.testing import CliRunner

from ..src.get_fixture_from_id_of_league import gffiol
from ..src.get_top_score_liga import app

runner = CliRunner()


def test_app():
    result = runner.invoke(app, ["get-topscorers", "--help"])
    assert result.exit_code == 0
    assert "[default: 2020]" in result.stdout
    assert "--league INTEGER" in result.stdout


def test_gffiol():
    result = runner.invoke(gffiol, ["--help"])
    assert result.exit_code == 0
    assert "[default: 135_2020]" in result.stdout
    assert "[default: no-is-free]" in result.stdout
