import requests
import json
from futbol import get_rapidkey, get_headers
import typer

rapidapi_key = get_rapidkey()
headers = get_headers()

app = typer.Typer(help="Awesome CLI user manager.")


@app.command()
def get_players_from_season(season: int = 2021, id_player: int = 306):
    conn = requests.get(
        "https://v3.football.api-sports.io/players?",
        params={"season": str(season), "id": str(id_player)},
        headers=headers,
    )
    conn.encoding = "utf-8"
    res = conn.json()
    with open(f"results/data_file_{id_player}_{season}.json", "w") as write_file:
        json.dump(res, write_file)


@app.command()
def get_squads_from_season(id_squad: int = 2315):
    conn = requests.get(
        "https://v3.football.api-sports.io/players/squads?",
        params={"team": str(id_squad)},
        headers=headers,
    )
    conn.encoding = "utf-8"
    res = conn.json()
    with open(f"results/data_file_{id_squad}.json", "w") as write_file:
        json.dump(res, write_file)


@app.command()
def get_squads_from_league(season: int = 2021, id_league: int = 240):
    conn = requests.get(
        "https://v3.football.api-sports.io/fixtures?",
        params={"season": str(season), "league": str(id_league)},
        headers=headers,
    )
    conn.encoding = "utf-8"
    res = conn.json()
    with open(f"results/data_file_{id_league}_{season}.json", "w") as write_file:
        json.dump(res, write_file)


@app.command()
def get_team_stats(id_squad: int = 2288, season: int = 2021, league_id: int = 262):
    conn = requests.get(
        "http://v3.football.api-sports.io/teams/statistics?",
        params={"season": season, "team": id_squad, "league": league_id},
        headers=headers,
    )
    res = conn.json()
    with open(f"results/data_file_{id_squad}_{season}.json", "w") as write_file:
        json.dump(res, write_file)


if __name__ == "__main__":
    app()
