import json
import requests
from futbol import get_rapidkey, get_headers

rapidapi_key = get_rapidkey()

headers = get_headers()

matches = [169101, 169102, 169103, 169104, 169105]

for match in matches:
    print(match)
    conn = requests.get(
        f"https://v3.football.api-sports.io/fixtures/statistics?fixture={match}",
        headers=headers,
    )

    conn.encoding = "utf-8"
    res = conn.json()
    home_team = res["response"][0]["team"]["name"]
    away_team = res["response"][1]["team"]["name"]
    with open(f"results/data_statisitcs_{match}.json", "w") as write_file:
        json.dump(res, write_file)
