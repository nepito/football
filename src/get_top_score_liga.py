import requests
import json
from futbol import get_rapidkey, get_headers
import typer

rapidapi_key = get_rapidkey()
headers = get_headers()

season = "2020"
league = "262"

app = typer.Typer(help="Awesome CLI user manager.")


@app.command()
def get_topscorers(season: int = 2020, league: int = 262):
    conn = requests.get(
        "https://v3.football.api-sports.io/players/topscorers",
        params={"season": season, "league": league},
        headers=headers,
    )
    conn.encoding = "utf-8"
    res = conn.json()
    with open(f"results/top_scorers_{league}_{season}.json", "w") as write_file:
        json.dump(res, write_file)


@app.command()
def get_players_from_team(season: int = 2021, league: int = 39, team: int = 33):
    conn = requests.get(
        "https://v3.football.api-sports.io/players",
        params={"season": season, "league": league, "team": team},
        headers=headers,
    )
    conn.encoding = "utf-8"
    res = conn.json()
    with open(f"results/players_from_{team}_at_{season}.json", "w") as write_file:
        json.dump(res, write_file)


@app.command()
def get_player_from_id(season: int = 2021, id_player: int = 306):
    conn = requests.get(
        "https://v3.football.api-sports.io/players",
        params={"season": season, "id": id_player},
        headers=headers,
    )
    conn.encoding = "utf-8"
    res = conn.json()
    with open(f"results/player_{id_player}_at_{season}.json", "w") as write_file:
        json.dump(res, write_file)
    print(res["response"][0]["player"]["name"])


if __name__ == "__main__":
    app()
