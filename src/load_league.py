import json

# Opening JSON file
league_raw = "results/data_file_262_2019.json"
f = open(
    league_raw,
)

# returns JSON object as
# a dictionary
league = json.load(f)
f.close()
# Iterating through the json
# lisot
teams_ids = []
match_ids = []
for match in league["response"]:
    was_played = match["fixture"]["status"]["short"] == "FT"
    if was_played:
        teams_ids.append(match["teams"]["home"]["id"])
        match_ids.append(match["fixture"]["id"])
unique_ids = list(set(teams_ids))
print("Los id de los equipos son:")
print(unique_ids)

unique_ids = list(set(match_ids))
number_of_matches = 50
print(f"Los id de los últimos {number_of_matches} juegos son:")
print(unique_ids[-number_of_matches:])
print(f"en total fueron {len(unique_ids)} juegos")


match_ids_to_team = []
id_team = 2287
for match in league["response"]:
    was_played = match["fixture"]["status"]["short"] == "FT"
    did_play_in_home = match["teams"]["home"]["id"] == id_team
    did_play_in_away = match["teams"]["away"]["id"] == id_team
    did_pay = (did_play_in_home | did_play_in_away) & was_played
    if did_pay:
        match_ids_to_team.append(match["fixture"]["id"])

unique_ids = list(set(match_ids_to_team))
print("Los id de los juegos son:")
print(unique_ids)
print(f"en total fueron {len(unique_ids)} juegos")
