import typer
from futbol import League, downladed_file, read_statistic_match, read_json, ID_LEAGUE_SEASON

app = typer.Typer(help="Awesome CLI user manager.")


@app.command()
def calculate_statistics_of_league(id_season: str = "262_2021"):
    league_path = f"results/data_file_{id_season}.json"
    statistic_of_league = read_json(league_path)
    league_season = ID_LEAGUE_SEASON[id_season]
    path_results = f"results/{league_season}"
    match_ids = downladed_file(path_results)
    write_statistic_mathces_league(statistic_of_league, match_ids, id_season, path_results)


def write_statistic_mathces_league(statistic_of_league, match_ids, id_season, path_results):
    league = League(statistic_of_league)
    for match_id in match_ids:
        print(match_id)
        path_match = f"{path_results}/data_statisitcs_{match_id}.json"
        statistic_of_match = read_statistic_match(path_match)
        league.append_match_statistic(statistic_of_match)
    league.write_statistics(f"results/statistics_{id_season}.csv")


if __name__ == "__main__":
    app()
