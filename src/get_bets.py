import pandas as pd
import requests
import json
import os
import time


def get_response_bet(league, match, season=2021):
    print(match)
    conn = requests.get(
        "http://v3.football.api-sports.io/odds?",
        params={"season": "2021", "league": league_id, "bet": 1, "bookmaker": 8, "fixture": match},
        headers=headers,
    )
    res = conn.json()
    return res["response"][0]["bookmakers"][0]["bets"][0]["values"]


rapid_key = os.environ["x_rapidapi_key"]
headers = {
    "x-rapidapi-host": "v3.football.api-sports.io",
    "x-rapidapi-key": rapid_key,
}
f = open("bets_inputs.json", "r")
a = f.read()
inputs = json.loads(a)
leagues_rounds = zip(inputs["league"], inputs["round"])
for league, round in leagues_rounds:
    league_season = f"{league}_2021"
    path_file = f"season_{league_season}.csv"
    matches = pd.read_csv(path_file)
    league_id = league_season.split("_")[0]
    round = round

    id_match = [match for match in matches[matches["round"].str.contains(f"- {round}")]["id_match"]]
    time.sleep(13)
    bets = [get_response_bet(league=league_id, match=match) for match in id_match]
    home = [bet[0]["odd"] for bet in bets]
    draw = [bet[1]["odd"] for bet in bets]
    away = [bet[2]["odd"] for bet in bets]

    dictionary = {
        "id_match": id_match,
        "home_bet": home,
        "draw_bet": draw,
        "away_bet": away,
    }

    with open(f"api_bets/data/bets_{league_season}_{round}.json", "w") as outfile:
        json.dump(dictionary, outfile)
