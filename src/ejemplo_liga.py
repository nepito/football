import requests
import json
from futbol import get_rapidkey, get_headers
import typer

rapidapi_key = get_rapidkey()
headers = get_headers()

app = typer.Typer(help="Awesome CLI user manager.")


@app.command()
def get_league_from_season_and_code(season: int = 2019, league: int = 262):
    conn = requests.get(
        "https://v3.football.api-sports.io/fixtures?",
        params={"season": str(season), "league": str(league)},
        headers=headers,
    )
    conn.encoding = "utf-8"
    res = conn.json()
    with open(f"results/data_file_{league}_{season}.json", "w") as write_file:
        json.dump(res, write_file)


if __name__ == "__main__":
    app()
