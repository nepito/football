import typer
from futbol import (
    Downloader_Matches_Minute_Minute,
    ID_LEAGUE_SEASON,
)
import time


def get_statistics_players_match(league_season, match, minute):
    downloader_matches = Downloader_Matches_Minute_Minute()
    downloader_matches.get_request(match)
    downloader_matches.write_match(league_season, minute)


gfmm = typer.Typer(help="Awesome CLI user manager.")


@gfmm.command()
def download_files(
    id_season: str = "39_2021",
    match: str = "710556",
    minutes: int = 45,
    is_second_time: bool = False,
):
    second_time = 0
    if is_second_time:
        second_time = 45
    league_season = ID_LEAGUE_SEASON[id_season]
    for minute in range(minutes):
        get_statistics_players_match(league_season, match, second_time + minute)
        print(f"minute {second_time + minute}")
        time.sleep(60)


if __name__ == "__main__":
    gfmm()
