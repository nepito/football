from futbol import League
import json

league_path = "results/data_file_262_2020.json"
league_file = open(league_path)
statistic_of_league = json.load(league_file)
league_file.close()


def read_statistic_match(match_path):
    match_file = open(match_path)
    statistic_of_match = json.load(match_file)
    match_file.close()
    return statistic_of_match


match_ids = [
    648845,
    648846,
    648847,
    648848,
    652913,
    652914,
    652915,
    652916,
    652917,
    652918,
    652919,
    652920,
    652921,
    652922,
    652924,
    652925,
    652926,
    652927,
    652928,
    652929,
    652930,
    652931,
    652932,
    652933,
    652936,
    652937,
    652938,
    652939,
    652940,
    652941,
    652945,
    652946,
    652947,
    652948,
    652949,
    652950,
    652951,
    652952,
    652953,
    652954,
]

league = League(statistic_of_league)
for match_id in match_ids:
    path_match = f"results/data_statisitcs_{match_id}.json"
    statistic_of_match = read_statistic_match(path_match)
    league.append_match_statistic(statistic_of_match)
league.write_statistics("results/statistics_262_2019.csv")
