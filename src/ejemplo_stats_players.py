import json
import requests
from futbol import get_rapidkey, get_headers

rapidapi_key = get_rapidkey()

headers = get_headers()

match = "169080"
conn = requests.get(
    f"https://v3.football.api-sports.io/fixtures/players?fixture={match}",
    headers=headers,
)

conn.encoding = "utf-8"
res = conn.json()
home_team = res["response"][0]["team"]["name"]
away_team = res["response"][1]["team"]["name"]
with open(f"results/data_statisitcs_players_{home_team}_{away_team}.json", "w") as write_file:
    json.dump(res, write_file)
