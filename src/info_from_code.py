import requests
import json
import os
import typer


rapid_key = os.environ["x_rapidapi_key"]
headers = {
    "x-rapidapi-host": "v3.football.api-sports.io",
    "x-rapidapi-key": rapid_key,
}

app = typer.Typer(help="Awesome CLI user manager.")


@app.command()
def get_league_from_season_and_code(season: int = 2019, code: str = "MX"):
    conn = requests.get(
        "https://v3.football.api-sports.io/leagues",
        params={"season": str(season), "code": code},
        headers=headers,
    )

    res = conn.json()
    with open(f"results/data_file_{code}_{season}.json", "w") as write_file:
        json.dump(res, write_file)


if __name__ == "__main__":
    app()
