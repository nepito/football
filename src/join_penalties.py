import typer
import pandas as pd
import numpy as np


app = typer.Typer(help="Awesome CLI user manager.")


@app.command()
def add_penalties_statistics(league_season: str = "39_2021"):
    results_path = f"results/statistics_{league_season}.csv"
    a = pd.read_csv(results_path)
    b = pd.read_csv("summary_penalties.csv")
    c = a.set_index("match_id").join(b.set_index("match_id"))
    c.to_csv(results_path)


if __name__ == "__main__":
    app()
