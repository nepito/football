import typer
import pandas as pd
from futbol import downladed_file, read_json, ID_LEAGUE_SEASON, Penalties_Team, Penalty_Match


app = typer.Typer(help="Awesome CLI user manager.")


def main(id_match, league_season) -> dict:
    path: str = f"results/{league_season}/data_statisitcs_players_{id_match}.json"
    players: dict = read_json(path)
    home_players: Penalties_Team = Penalties_Team(players["response"][0]["players"])
    away_players: Penalties_Team = Penalties_Team(players["response"][1]["players"])
    penalty_match = {
        "away_scored": away_players.scored(),
        "away_missed": away_players.missed(),
        "home_scored": home_players.scored(),
        "home_missed": home_players.missed(),
        "id_match": id_match,
    }
    return penalty_match


@app.command()
def get_penalties(league_season: str = "39_2021"):
    league_season = ID_LEAGUE_SEASON[league_season]
    path_results = f"results/{league_season}"
    match_ids = downladed_file(path_results)
    penalties = [Penalty_Match(**main(id, league_season)) for id in match_ids]
    total_total = sum([penalty.home_total + penalty.away_total for penalty in penalties])
    missed_total = sum([penalty.home_missed + penalty.away_missed for penalty in penalties])
    p = (total_total - missed_total) / total_total
    print(p)
    summary = {
        "home_scored_penalties": [penalty.home_scored for penalty in penalties],
        "away_scored_penalties": [penalty.away_scored for penalty in penalties],
        "home_total_penalties": [penalty.home_total for penalty in penalties],
        "away_total_penalties": [penalty.away_total for penalty in penalties],
        "match_id": [penalty.id_match for penalty in penalties],
    }
    df_summary = pd.DataFrame.from_dict(summary)
    df_summary.to_csv("summary_penalties.csv", index=False)


if __name__ == "__main__":
    app()
