import pandas as pd
from futbol import (
    downladed_file,
    read_json,
    ID_LEAGUE_SEASON,
)
import tidyball as tb


def init_players() -> pd.DataFrame:
    d = {
        "match": [],
        "team": [],
        "player": [],
        "minutes": [],
        "position": [],
        "number": [],
        "goal_total": [],
        "goal_conceded": [],
        "goal_assists": [],
        "goal_saves": [],
        "passes_total": [],
        "passes_key": [],
        "passes_accuracy": [],
    }
    players = pd.DataFrame(d)
    return players


def get_statistic_mathces_players(id_season, match):
    league_path = f"results/{ID_LEAGUE_SEASON[id_season]}/data_statisitcs_players_{match}.json"
    data = read_json(league_path)
    players_in_match = tb.get_goals_passes_tackles_and_dribbles_statistic_from_match(data)
    return players_in_match


def get_id_of_mathced_dowloared(id_season) -> list:
    league_season = ID_LEAGUE_SEASON[id_season]
    path_results = f"results/{league_season}"
    match_ids = downladed_file(path_results)
    return match_ids


id_season = "39_2021"

if __name__ == "__main__":
    players = init_players()
    matches = get_id_of_mathced_dowloared(id_season)
    for match in matches:
        players = pd.concat([players, get_statistic_mathces_players(id_season, match)])
    players_path = f"results/{ID_LEAGUE_SEASON[id_season]}/statisitcs_players_{id_season}.csv"
    players.to_csv(players_path, index=False)
