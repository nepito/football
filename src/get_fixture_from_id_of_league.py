import typer
from futbol import (
    Downloader_Matches,
    Downloader_Players,
    read_json,
    get_id_played_match,
    downladed_file,
    ID_LEAGUE_SEASON,
)
import time


def get_statistics_players_match(for_download, league_season, is_free):
    downloader_matches = Downloader_Matches()
    downloader_players = Downloader_Players()
    for match in for_download:
        print(match)
        downloader_matches.get_request(match)
        downloader_matches.write_match(league_season)
        downloader_players.get_request(match)
        downloader_players.write_players(league_season)
        if is_free:
            time.sleep(13)


gffiol = typer.Typer(help="Awesome CLI user manager.")


@gffiol.command()
def download_files(id_season: str = "135_2020", is_free: bool = False):
    league_raw = f"results/data_file_{id_season}.json"
    league = read_json(league_raw)
    league_season = ID_LEAGUE_SEASON[id_season]

    match_ids = get_id_played_match(league)
    downladed_match = downladed_file(f"results/{league_season}")
    not_downloaded_yet = [id for id in match_ids if id not in downladed_match]

    get_statistics_players_match(not_downloaded_yet, league_season, is_free)


if __name__ == "__main__":
    gffiol()
