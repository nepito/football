import pandas as pd
from futbol import (
    read_json,
    return_dictionary_from_season,
    Match_Round,
    return_dictionary_from_match,
)

league_season = "262_2021"
path_season = f"results/data_file_{league_season}.json"

if __name__ == "__main__":
    season = read_json(path_season)
    matches = season["response"]
    variables = return_dictionary_from_match(matches[0])
    season = [Match_Round(**return_dictionary_from_match(match)) for match in matches]
    match = Match_Round(**variables)
    all_data = return_dictionary_from_season(season)
    df_all_data = pd.DataFrame.from_dict(all_data)
    df_all_data.to_csv(f"season_{league_season}.csv", index=False)
    print(season[0])
